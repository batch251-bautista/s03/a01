# Create a class called "Camper" and give it the attributes name, batch, course, type
class Camper():
    def __init__(self, name, batch, course, type):
        self.name = name
        self.batch = batch
        self.course = course
        self.type = type

# Create a method called "career_track" which will print put the string "Currently enrolled in the <value of course_type> program"
    def career_track(self):
        print(f"Currently enrolled in the {self.type} program")

# Create a method called info which will print out the string "My name is <value of name> of batch <value of batch>"
    def info(self):
        print(f"My name is {self.name} of Batch {self.batch}.")

# Create an object from class Camper called zuitt_camper and pass in arguments for name, batch, and course type
zuitt_camper = Camper("Malthael", "01", "Navy SEAL", "Survivalist")

# Print the value of the objects name, batch, course type
print(f"Reporting for duty, {zuitt_camper.name}, Batch {zuitt_camper.batch}, {zuitt_camper.course}, {zuitt_camper.type}")

# Execute the info method and career_track of the object
zuitt_camper.info()
zuitt_camper.career_track()